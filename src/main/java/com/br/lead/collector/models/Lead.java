package com.br.lead.collector.models;

import com.br.lead.collector.enums.TipoLead;

public class Lead {

    private String nome;
    private String email;
    private TipoLead tipoLead;

    public Lead() {
    }

    public Lead(String nome, String email, TipoLead tipoLead) {
        this.nome = nome;
        this.email = email;
        this.tipoLead = tipoLead;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public TipoLead getTipoLead() {
        return tipoLead;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTipoLead(TipoLead tipoLead) {
        this.tipoLead = tipoLead;
    }
}
