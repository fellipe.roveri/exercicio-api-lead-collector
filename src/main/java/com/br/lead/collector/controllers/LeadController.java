package com.br.lead.collector.controllers;

import com.br.lead.collector.enums.TipoLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/lead")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @GetMapping("/{indice}")
    public Lead buscarLead(@PathVariable int indice){
        Lead lead;
        try{
            lead = leadService.buscarPorIndice(indice);

        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "O índice não existe em nosso banco de dados");
        }
        return lead;

    }

    @PostMapping
    public ResponseEntity<Lead> incluir(@RequestBody Lead lead) {
        Lead leadObjeto = leadService.adicionar(lead);
        return ResponseEntity.status(201).body(leadObjeto);
    }

}
